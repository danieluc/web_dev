class CreateKelas < ActiveRecord::Migration[5.0]
  def change
    create_table :kelas do |t|
      t.string :MataKuliah
      t.string :SKS
      t.string :jam
      t.string :ruangan
      t.string :hari
      t.integer :jumlahMahasiswa

      t.timestamps
    end
  end
end
